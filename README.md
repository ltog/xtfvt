# xtfvt

XTF Validation Tool -- Validates INTERLIS 2 .xtf transfer files

## Project status

The development of xtfvt is stopped for the time being.

## Description

This is the place where [I](http://www.htwchur.ch/ueber-uns/ansprechpartner/mitarbeitende/einzelansicht.html?no_cache=1&tx_htwaddress_pi1[letter]=T&tx_htwaddress_pi1[pointer]=0&tx_htwaddress_pi1[showUid]=674&tx_htwaddress_pi1[backPid]=532) will work on a validation tool for [INTERLIS 2](http://www.interlis.ch) transfer files as part of my master thesis. For lack of a better name the tool is called "xtfvt" (standing for "xtf validation tool").

It will be possible to run the tool in two modes: Either integrated in JHOVE2 or as stand-alone application.

The software is based on Java and should therefore run on any platform that supports Java. However, there are some helper scripts, which were developed for GNU/Linux and are only partially ported to Windows. Use Linux for the best experience!

Read more about [JHOVE2](https://bitbucket.org/jhove2/main/wiki/Home). The code for integrating xtfvt into JHOVE2 is maintained [here](https://github.com/ltog/jhove2).

## Setup

Things are still quite rough on the edges and the installation is a bit complex. In the future this steps will get a bit easier.

### Setup steps for stand-alone mode

Download the xtfvt source code:

`git clone https://github.com/ltog/xtfvt.git`

Compile:

Call `build.sh` (GNU/Linux) or `build.bat` (Windows, may need adjustments to JAVA_HOME)

**Note:** On Ubuntu 14.10 you might need the following packages (install using `sudo apt-get install ...`):

- openjdk-7-jdk
- git
- maven

If you are using VMware Player, the VMware Tools (for convenience features such as adaptive resizing) can be handily installed by installing the package `open-vm-tools-desktop`.

### Setup steps for JHOVE2 mode (currently Linux only)

Perform steps for stand-alone mode (see above).

Download the source code of my JHOVE2 fork:

`git clone https://github.com/ltog/jhove2.git` (perform this step in the same directory as you did for the xtfvt source code)

Build the project and extract the resulting zip file:

Call `compile_and_extract.sh`

## Usage

### Getting .xtf files

If you haven't got your own .xtf files, there is a downloader script in the root of the xtfvt repository.

Call `download-external-xtfs-for-linux.sh` (sorry, the Windows script is outdated)

Files will be downloaded to `src/test/resources/ch/htwchur/xtfvt/checks/external/`.

### Usage in stand-alone mode

`java -cp "target/classes/*:target/xtfvt-0.0.1-SNAPSHOT.jar" ch.htwchur.xtfvt.XtfvtMain -x /path/to/xtf_file [[-i /path/to/ili_file [-i ... ]] | [-r http://models.myrepo.ch/ [-r ...]]]`

or as a shortcut when only giving the path to the .xtf file and let xtfvt detect the .ili file itself (in the same directory):

`run.sh /path/to/xtf_file` (Linux only)

(This in fact just uses the above command and passes the `-x` option through.)

### Usage in JHOVE2 mode (currently Linux only)

`call_jhove2.sh /path/to/xtf_file` (Linux only)

## Features

* Validation by XML schema using ili2c
* Auto-detection of model files
* In-depth validation of .xtf files with a focus on topology
