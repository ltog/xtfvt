package ch.htwchur.xtfvt;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * adapted from
 * - http://stackoverflow.com/a/25021229
 * 
 * other sources
 * - http://piotrga.wordpress.com/2009/03/27/hamcrest-regex-matcher/
 * - http://www.flamingpenguin.co.uk/blog/2012/01/13/hamcrest-regexp-matcher/
 * 
 * @author ltog
 */

public class RegexMatcher extends TypeSafeMatcher<String> {

    private final String regex;

    public RegexMatcher(final String regex) {
        this.regex = regex;
    }

    public void describeTo(final Description description) {
        description.appendText("matches regex='" + regex + "'");
    }

    @Override
    public boolean matchesSafely(final String input) {
    	Pattern p = Pattern.compile(regex, Pattern.DOTALL);
    	Matcher m = p.matcher(input);
    	return m.matches();
    	// returning String.matches(regex) is not suitable since .* would then not match newline characters in multiline inputs; see also Pattern.DOTALL
    }

    public static RegexMatcher matchesRegex(final String regex) {
        return new RegexMatcher(regex);
    }
}