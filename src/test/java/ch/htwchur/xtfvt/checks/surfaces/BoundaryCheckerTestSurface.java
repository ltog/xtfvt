package ch.htwchur.xtfvt.checks.surfaces;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class BoundaryCheckerTestSurface extends AbstractTest {

	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";

	final String superdirectory = "intersecting_boundary_lines/surface/";

	// --- Straight Straight Tests ---------------------------------------------
	@Test
	public void testStraightStraightIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_straight/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}

	@Test
	public void testStraightStraightIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_straight/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	// --- Straight Arc Tests --------------------------------------------------
	@Test
	public void testStraightArcIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and CIRCULARSTRING(1.0 1.0, 0.6 0.5, 0.0 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}

	@Test
	public void testStraightArcStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and CIRCULARSTRING(1.0 1.0, 0.6 0.5, 0.0 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Arc Straight Tests --------------------------------------------------
	@Test
	public void testArcStraightIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and CIRCULARSTRING(1.0 1.0, 0.6 0.5, 0.0 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}

	@Test
	public void testArcStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = superdirectory+"straight_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: LINESTRING (0 1, 1 0) and CIRCULARSTRING(1.0 1.0, 0.6 0.5, 0.0 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Arc Arc Tests --------------------------------------------------
	@Test
	public void testArcArcIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"arc_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: CIRCULARSTRING(0.0 0.0, 0.4 0.5, 0.0 1.0) and CIRCULARSTRING(0.6 1.0, 0.2 0.5, 0.6 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}

	@Test
	public void testArcArcIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = superdirectory+"arc_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same boundary: CIRCULARSTRING(0.0 0.0, 0.4 0.5, 0.0 1.0) and CIRCULARSTRING(0.6 1.0, 0.2 0.5, 0.6 0.0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
}