package ch.htwchur.xtfvt.checks.surfaces;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SurfaceCheckerTest extends AbstractTest {

	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	final String superdirectory = "intersection_between_boundaries/";
	
	// --- Area Tests ------------------------------------------------
	
	@Test
	public void testIntersectionBetweenBoundariesInAreaWithoutIli() throws URISyntaxException {
		final String directory = superdirectory + "area/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and LINESTRING (0.1 0.5, 0.5 1.5) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and CIRCULARSTRING(0.5 1.5, 0.7 1.0, 0.5 0.5) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testIntersectionBetweenBoundariesInAreaWithIli() throws URISyntaxException {
		final String directory = superdirectory + "area/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and LINESTRING (0.1 0.5, 0.5 1.5) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and CIRCULARSTRING(0.5 1.5, 0.7 1.0, 0.5 0.5) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	
	@Test
	public void testIntersectionBetweenBoundariesInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = superdirectory + "surface/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and LINESTRING (0.1 0.5, 0.5 1.5) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and CIRCULARSTRING(0.5 1.5, 0.7 1.0, 0.5 0.5) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testIntersectionBetweenBoundariesInSurfaceWithIli() throws URISyntaxException {
		final String directory = superdirectory + "surface/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and LINESTRING (0.1 0.5, 0.5 1.5) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
			add(new IsEqual<String>("Intersecting boundary segments LINESTRING (0 1, 1 1) and CIRCULARSTRING(0.5 1.5, 0.7 1.0, 0.5 0.5) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	

}
