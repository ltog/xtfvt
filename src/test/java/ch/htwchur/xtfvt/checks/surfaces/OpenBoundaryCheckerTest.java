package ch.htwchur.xtfvt.checks.surfaces;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class OpenBoundaryCheckerTest extends AbstractTest {

	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	// --- Area Tests ------------------------------------------------
	
	@Test
	public void testOpenBoundaryInAreaWithoutIli() throws URISyntaxException {
		final String directory = "open_boundary/area/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Open boundary at point POINT (1 1.3) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
			add(new IsEqual<String>("Open boundary at point POINT (1 1) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testOpenBoundaryInAreaWithIli() throws URISyntaxException {
		final String directory = "open_boundary/area/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Open boundary at point POINT (1 1.3) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
			add(new IsEqual<String>("Open boundary at point POINT (1 1) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	
	@Test
	public void testOpenBoundaryInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "open_boundary/surface/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Open boundary at point POINT (1 1.3) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
			add(new IsEqual<String>("Open boundary at point POINT (1 1) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testOpenBoundaryInSurfaceWithIli() throws URISyntaxException {
		final String directory = "open_boundary/surface/";
		@SuppressWarnings("serial")
		final List<Matcher<String>> errors = new ArrayList<Matcher<String>>() {{
			add(new IsEqual<String>("Open boundary at point POINT (1 1.3) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
			add(new IsEqual<String>("Open boundary at point POINT (1 1) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57"));
		}};
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	

}
