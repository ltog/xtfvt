package ch.htwchur.xtfvt.checks.segments;

import java.net.URISyntaxException;
import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class CurveSegmentCheckerTest extends AbstractTest {
	
	private final String xtfPath = "BasicModel.xtf";
	private final String iliPath = "BasicModel.ili";
	
	final Matcher<String> msg = new IsEqual<String>(
			"Duplicate C1/C2 pairs in consecutive curve segments; C1=1.0 C2=1.0 in BasicModel.BasicTopic.MyAreaClass.Geometry OID=Area1 near line 15 column 51");
	
	@Test
	public void testWithoutIli() throws URISyntaxException {
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(xtfPath));
	}
	
	@Test
	public void testWithIli() throws URISyntaxException {
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(xtfPath), path2Uri(iliPath));
	}
}
