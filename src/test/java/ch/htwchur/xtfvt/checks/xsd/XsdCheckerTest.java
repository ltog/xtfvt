package ch.htwchur.xtfvt.checks.xsd;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class XsdCheckerTest extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	@Test
	public void testEmptyXtfWithoutIli() throws URISyntaxException {
		final String directory = "empty_xtf/";
		Matcher<String> error = new RegexMatcher("^ERROR in xtfvt's class Compiler: could not detect a model in file .*empty_xtf[\\/]BasicModel\\.xtf Exception: ch\\.interlis\\.iox\\.IoxException: javax\\.xml\\.stream\\.XMLStreamException: ParseError at \\[row,col\\]:\\[1,1\\]\\\nMessage: Premature end of file\\.$");
		TestHelper.assertCorrectValidation(XtfValidationResult.UNDECIDED, error, path2Uri(directory + xtfName));
	}
	
	@Test
	public void testEmptyXtfWithIli() throws URISyntaxException {
		final String directory = "empty_xtf/";
		Matcher<String> error = new RegexMatcher("^ERROR in xtfvt's class XsdChecker: xml not valid in .*empty_xtf[\\/]BasicModel\\.xtf$");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, error, path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	@Test
	public void testBlankLinesWithoutIli() throws URISyntaxException {
		final String directory = "blank_lines_in_xtf/";
		Matcher<String> error = new RegexMatcher("^ERROR in xtfvt's class Compiler: could not detect a model in file .*blank_lines_in_xtf[\\/]BasicModel\\.xtf Exception: ch\\.interlis\\.iox\\.IoxException: javax\\.xml\\.stream\\.XMLStreamException: ParseError at \\[row,col\\]:\\[4,6\\]\\\nMessage: The processing instruction target matching .*xX.*mM.*lL.* is not allowed.");	
		TestHelper.assertCorrectValidation(XtfValidationResult.UNDECIDED, error, path2Uri(directory + xtfName));
	}
	
	@Test
	public void testBlankLinesWithIli() throws URISyntaxException {
		final String directory = "blank_lines_in_xtf/";
		Matcher<String> error = new RegexMatcher("^ERROR in xtfvt's class XsdChecker: xml not valid in .*blank_lines_in_xtf[\\/]BasicModel\\.xtf$");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, error, path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
}
