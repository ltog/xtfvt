package ch.htwchur.xtfvt.checks.external.lookup;

import java.net.URISyntaxException;

import org.junit.Test;

import ch.htwchur.xtfvt.AbstractTest;
import ch.htwchur.xtfvt.TestHelper;

public class LookupTest extends AbstractTest {
	
	private final String xtfPath = "LookUp_ili2_v1.3.xtf";
	private final String iliPath = "LookUp_ili2_v1.3.ili";
	
	@Test
	public void testWithoutIli() throws URISyntaxException {
		assumeNotNullExternalFile(xtfPath);
		TestHelper.assertIsWithoutError(path2Uri(xtfPath));
	}
	
	@Test
	public void testWithIli() throws URISyntaxException {
		assumeNotNullExternalFile(xtfPath, iliPath);
		TestHelper.assertIsWithoutError(path2Uri(xtfPath),path2Uri(iliPath));
	}

}