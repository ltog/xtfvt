package ch.htwchur.xtfvt.checks.external.roads;

import java.net.URISyntaxException;

import org.junit.Test;

import ch.htwchur.xtfvt.AbstractTest;
import ch.htwchur.xtfvt.TestHelper;

public class RoadsExampleTest extends AbstractTest {
	
	private final String xtfPath  = "RoadsExdm2ien-20050616.xml";
	private final String iliPath1 = "RoadsExdm2ien-20050616.ili";
	private final String iliPath2 = "RoadsExdm2ben-20050616.ili";
	
	@Test
	public void testWithoutIli() throws URISyntaxException {
		assumeNotNullExternalFile(xtfPath);
		TestHelper.assertIsWithoutError(path2Uri(xtfPath));
	}
	
	@Test
	public void testWithIli() throws URISyntaxException {
		assumeNotNullExternalFile(xtfPath, iliPath1, iliPath2);
		TestHelper.assertIsWithoutError(
				path2Uri(xtfPath),path2Uri(iliPath1),path2Uri(iliPath2));
	}

}