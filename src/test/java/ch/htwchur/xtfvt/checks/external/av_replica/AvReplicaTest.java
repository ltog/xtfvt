package ch.htwchur.xtfvt.checks.external.av_replica;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class AvReplicaTest extends AbstractTest {
	
	private final String xtfPath  = "Test23_erweitert.xml";
	private final String iliPath1 = "Test23.ili";
	private final String iliPath2 = "Test23_erweitert.ili";
	private final String iliPath3 = "Time.ili";
	private final String iliPath4 = "Units.ili";
	
	private String errorRegexString = "^Radius given in circle arc segment is too small.*$";
	
	
	@Test
	public void testWithoutIli() throws URISyntaxException {
		List<Matcher<String>> errors = new ArrayList<Matcher<String>>();
	
		for (int i=0; i<3100; i++) {
			errors.add(new RegexMatcher(errorRegexString));
		}
		
		assumeNotNullExternalFile(xtfPath);
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(xtfPath));
	}
	
	@Test
	public void testWithIli() throws URISyntaxException {
		List<Matcher<String>> errors = new ArrayList<Matcher<String>>();
	
		for (int i=0; i<3100; i++) {
			errors.add(new RegexMatcher(errorRegexString));
		}
		
		assumeNotNullExternalFile(xtfPath, iliPath1, iliPath2, iliPath3, iliPath4);
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, errors,
				path2Uri(xtfPath), path2Uri(iliPath1), path2Uri(iliPath2),
				path2Uri(iliPath3), path2Uri(iliPath4));
	}

}