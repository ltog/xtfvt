package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestArcStraight extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	final String superdirectory = "arc_straight_intersection/";
	
	// === Tests for intersection of arc segment with straight segment ===
	// --- Area Tests ------------------------------------------------
	@Test
	public void testArcStraightIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testArcStraightIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = superdirectory+"area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Polyline Tests ------------------------------------------------
	@Test
	public void testArcStraightIntersectionInPolylineWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testArcStraightIntersectionInPolylineWithIli() throws URISyntaxException {
		final String directory = superdirectory+"polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	@Test
	public void testArcStraightIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = superdirectory+"surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testArcStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = superdirectory+"surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: CIRCULARSTRING(0.0 0.0, 0.5 0.4, 1.0 0.0) and LINESTRING (1 0.3, 0 0.3) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
