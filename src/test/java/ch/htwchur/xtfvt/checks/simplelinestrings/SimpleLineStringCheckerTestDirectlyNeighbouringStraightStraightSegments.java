package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestDirectlyNeighbouringStraightStraightSegments extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	
	// === Tests for intersection of directly neighbouring straight segments ===
	
	// --- Area Tests ------------------------------------------------
	@Test
	public void testNeighbouringStraightStraightIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringStraightStraightIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Polyline Tests ------------------------------------------------
	@Test
	public void testNeighbouringStraightStraightIntersectionInPolylineWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringStraightStraightIntersectionInPolylineWithIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	@Test
	public void testNeighbouringStraightStraightIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringStraightStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = "neighbouring_straight_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected straight segments: LINESTRING (0 0, 0 1.2) and LINESTRING (0 1.2, 0 1) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
