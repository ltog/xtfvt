package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestStraightStraight extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	
	
	// === Tests for intersection of straight segment with straight segment ===
	
	// --- Area Tests ------------------------------------------------
	@Test
	public void testStraightStraightIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testStraightStraightIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Polyline Tests ------------------------------------------------
	@Test
	public void testStraightStraightIntersectionInPolylineWithoutIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testStraightStraightIntersectionInPolylineWithIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	@Test
	public void testStraightStraightIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testStraightStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = "straight_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two segments of the same line string: LINESTRING (0 1, 1 0) and LINESTRING (1 1, 0 0) in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 85 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
