package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestDirectlyNeighbouringArcArcSegments extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	
	
	// === Tests for intersection of directly neighbouring arc segments ===
	// --- Area Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcArcIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcArcIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Polyline Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcArcIntersectionInPolylineWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcArcIntersectionInPolylineWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcArcIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 73 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcArcIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_arc_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 0.3 0.5, 0.0 1.0) and CIRCULARSTRING(0.0 1.0, -0.3 0.5, 0.1 0.0), maximum allowed overlap=0.01, actual overlap= 0.5999786595084413 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface1 near line 73 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
