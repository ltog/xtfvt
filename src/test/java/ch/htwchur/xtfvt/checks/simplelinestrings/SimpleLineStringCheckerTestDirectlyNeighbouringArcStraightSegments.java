package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestDirectlyNeighbouringArcStraightSegments extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	
	// === Tests for intersection of directly neighbouring arc and straight segments ===
	
	// --- Area Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcStraightIntersectionInAreaWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcStraightIntersectionInAreaWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Polyline Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcStraightIntersectionInPolylineWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcStraightIntersectionInPolylineWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests ------------------------------------------------
	@Test
	public void testNeighbouringArcStraightIntersectionInSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface2 near line 114 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testNeighbouringArcStraightIntersectionInSurfaceWithIli() throws URISyntaxException {
		final String directory = "neighbouring_arc_straight_intersection/surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Intersection between two directly connected segments (at least one of them is an arc segment): CIRCULARSTRING(0.0 0.0, 1.0 0.0, 0.5 -1.0) and LINESTRING (0.5 -1, 3 0), maximum allowed overlap=0.01, actual overlap= 0.044702066382294854 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface2 near line 114 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
