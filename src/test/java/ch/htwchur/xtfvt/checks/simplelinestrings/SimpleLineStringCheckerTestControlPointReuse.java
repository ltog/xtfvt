package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class SimpleLineStringCheckerTestControlPointReuse extends AbstractTest {
	
	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	// === Tests for control point reuse =============================
	
	// --- Area Tests ------------------------------------------------
	
	@Test
	public void testControlPointReuseAreaWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseAreaWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReuseAreaArcWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseAreaArcWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReuseAreaArcRadiusWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseAreaArcRadiusWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	// --- Polyline Tests ------------------------------------------------
	
	@Test
	public void testControlPointReusePolylineWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReusePolylineWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReusePolylineArcWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReusePolylineArcWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReusePolylineArcRadiusWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReusePolylineArcRadiusWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	// --- Surface Tests ------------------------------------------------
	
	@Test
	public void testControlPointReuseSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseSurfaceWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReuseSurfaceArcWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseSurfaceArcWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	@Test
	public void testControlPointReuseSurfaceArcRadiusWithoutIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testControlPointReuseSurfaceArcRadiusWithIli() throws URISyntaxException {
		final String directory = "control_point_reuse/area_arc_radius/";
		final Matcher<String> msg = new IsEqual<String>(
				"Polyline reuses control point with C1=1.0 and C2=1.0 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area1 near line 15 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
}
