package ch.htwchur.xtfvt.checks.valid;

import java.net.URISyntaxException;

import org.junit.Test;

import ch.htwchur.xtfvt.AbstractTest;
import ch.htwchur.xtfvt.TestHelper;

public class BasicModelTest extends AbstractTest {
	
	private final String xtfPath = "BasicModel.xtf";
	private final String iliPath = "BasicModel.ili";
	
	@Test
	public void testWithoutIli() throws URISyntaxException {
		TestHelper.assertIsWithoutError(path2Uri(xtfPath));
	}
	
	@Test
	public void testWithIli() throws URISyntaxException {
		TestHelper.assertIsWithoutError(path2Uri(xtfPath),path2Uri(iliPath));
	}
}
