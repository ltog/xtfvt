package ch.htwchur.xtfvt.checks.arcsegments;

import java.net.URISyntaxException;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import ch.htwchur.xtfvt.AbstractTest;
import ch.htwchur.xtfvt.TestHelper;
import ch.htwchur.xtfvt.datastructures.XtfValidationResult;

public class ArcSegmentCheckerTest extends AbstractTest {

	private final String xtfName = "BasicModel.xtf";
	private final String iliName = "BasicModel.ili";
	
	// --- Area Tests ----------------------------------------------------------
	@Test
	public void testAreaWithoutIli() throws URISyntaxException {
		final String directory = "small_r_in_area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=2.0, C2=0.0, previous C1=2.0, previous C2=1.0, R=0.1 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area2 near line 44 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testAreaWithIli() throws URISyntaxException {
		final String directory = "small_r_in_area/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=2.0, C2=0.0, previous C1=2.0, previous C2=1.0, R=0.1 in BasicModel.BasicTopic.MyAreaClass.AreaGeometry OID=Area2 near line 44 column 51");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}

	// --- Polyline Tests ------------------------------------------------------
	
	@Test
	public void testPolylineWithoutIli() throws URISyntaxException {
		final String directory = "small_r_in_polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=0.0, C2=0.0, previous C1=1.0, previous C2=0.0, R=0.1 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testPolylineWithIli() throws URISyntaxException {
		final String directory = "small_r_in_polyline/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=0.0, C2=0.0, previous C1=1.0, previous C2=0.0, R=0.1 in BasicModel.BasicTopic.MyPolylineClass.PolylineGeometry OID=Polyline1 near line 73 column 59");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
	// --- Surface Tests -------------------------------------------------------
	@Test
	public void testSurfaceWithoutIli() throws URISyntaxException {
		final String directory = "small_r_in_surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=2.0, C2=0.0, previous C1=2.0, previous C2=1.0, R=0.1 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface2 near line 114 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName));
	}
	
	@Test
	public void testSurfaceWithIli() throws URISyntaxException {
		final String directory = "small_r_in_surface/";
		final Matcher<String> msg = new IsEqual<String>(
				"Radius given in circle arc segment is too small; C1=2.0, C2=0.0, previous C1=2.0, previous C2=1.0, R=0.1 in BasicModel.BasicTopic.MySurfaceClass.SurfaceGeometry OID=Surface2 near line 114 column 57");
		TestHelper.assertCorrectValidation(XtfValidationResult.FAIL, msg,
				path2Uri(directory + xtfName), path2Uri(directory + iliName));
	}
	
}
