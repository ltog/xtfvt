package ch.htwchur.xtfvt;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;

import ch.htwchur.xtfvt.datastructures.*;

import com.beust.jcommander.JCommander;

public final class TestHelper {

	private static final List<Matcher<String>> noErrors = new ArrayList<Matcher<String>>();

	/* Here we have some different function signatures that in the end all point
	 * to the same method that does the comparison between the expected and the
	 * actual list of errors.
	 */
	
	public static void assertIsWithoutError(URI xtfUri) {
		assertCorrectValidation(XtfValidationResult.PASS, noErrors, xtfUri);
	}
	
	public static void assertIsWithoutError(URI xtfUri, URI...iliUris) {
		assertCorrectValidation(XtfValidationResult.PASS, noErrors, xtfUri, iliUris);
	}
	
	public static void assertCorrectValidation(XtfValidationResult expectedResult,
			List<Matcher<String>> errors, URI xtfUri) {
		assertCorrectValidation(expectedResult, errors, xtfUri, (URI[])null);
	}
	
	public static void assertCorrectValidation(XtfValidationResult expectedResult,
			Matcher<String> error, URI xtfUri) {
		List<Matcher<String>> errors = new ArrayList<Matcher<String>>();
		errors.add(error);
		assertCorrectValidation(expectedResult, errors, xtfUri, (URI[])null);
	}
	
	public static void assertCorrectValidation(XtfValidationResult expectedResult,
			Matcher<String> error, URI xtfUri, URI... iliUris) {
		List<Matcher<String>> errors = new ArrayList<Matcher<String>>();
		errors.add(error);
		assertCorrectValidation(expectedResult, errors, xtfUri, iliUris);
	}
	
	public static void assertCorrectValidation(XtfValidationResult expectedResult,
			List<Matcher<String>> expectedErrors, URI xtfUri, URI... iliUris) {
	
		// simulate command line arguments
		List<String> args = new ArrayList<String>();
		args.add("-x");
		args.add(xtfUri.getPath());
		if (iliUris != null) {
			for (URI uri: iliUris) {
				args.add("-i");
				args.add(uri.getPath());
			}
		}

		Settings settings = new Settings();
		/* The command line parameters will be written to the settings object. */
		new JCommander(settings, args.toArray(new String[args.size()]));
		
		Xtfvt xtfvt = new Xtfvt(settings);

		XtfValidationResultWithErrorList actualResult = null;
		try {
			actualResult = xtfvt.validate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		// we need this part, since otherwise for an unexpectedly failing test
		// JUnit would not give error messages, but only FAIL, which is hard to debug 
		if (expectedResult == XtfValidationResult.PASS &&
				actualResult.getErrors().size() > 0) {
			fail(actualResult.getErrors().toString());
		}
		
		// compare validation results, e.g. FAIL
		assertThat(expectedResult, equalTo(actualResult.getXtfValidationResult()));

		// the list of expected and actual errors must have the same number of elements
		if (expectedErrors.size() != actualResult.getErrors().size()) {
			for (String error : actualResult.getErrors()) {
				System.out.println(error);
			}
		}
		assertThat(expectedErrors.size(), equalTo(actualResult.getErrors().size()));
	
		// iterate over expected errors and compare with actual errors
		for (int i=0; i<expectedErrors.size(); i++) {
			Matcher<String> expectedError = expectedErrors.get(i);
			
			// actual error must be String
			assertThat(actualResult.getErrors().get(i), is(instanceOf(String.class)));
			
			String actualError = actualResult.getErrors().get(i);
			
			assertThat(actualError, expectedError);
		}
	}
}
