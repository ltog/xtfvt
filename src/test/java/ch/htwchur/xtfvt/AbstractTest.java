package ch.htwchur.xtfvt;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assume;

public abstract class AbstractTest {
	
	protected URI path2Uri (String path) throws URISyntaxException {
		return this.getClass().getResource(path).toURI();
	}
	
	protected void assumeNotNullExternalFile(String... paths) {
		for (String path: paths) {
			Assume.assumeNotNull(
					"The file " + path + " needs to be downloaded separately. Use the provided batch file.",
					getClass().getResource(path));
		}
	}
}