package ch.htwchur.xtfvt.checks.simplelinestrings;

import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.*;
import ch.htwchur.xtfvt.helpers.*;
import static ch.htwchur.xtfvt.helpers.IomBrowser.*;
import ch.interlis.iom.IomObject;

/**
 * Checker for simple line strings;
 * Implements requirements SLS1, SLS2, SLS3, SLS4, SLS5
 * 
 * @author ltog
 *
 */
public class SimpleLineStringChecker extends CheckerWithXtfReader{
	
	private MTCN2MaxOverlap mtcn2MaxOverlap;

	public SimpleLineStringChecker(
			final String path,
			final MTCN2MaxOverlap mtcn2MaxOverlap) {
		
		super(path);
		this.mtcn2MaxOverlap = mtcn2MaxOverlap;
	}

	@Override
	protected void handleObjectEvent(final IomObject iomObject) {
		
		List<IomObjectWithInfo> polylines =
				getRecursivelyObjectsWithInfoHavingTag(iomObject, "POLYLINE");

		for (IomObjectWithInfo polyline : polylines) {
			final String modelTopicClassName = polyline.getModelTopicClassName();
			
			Double maxOverlap = mtcn2MaxOverlap.get(modelTopicClassName);

			// the current polyline object should be a _simple_ line string
			if (maxOverlap != null) {

				final List<IomObject> vertices = IomBrowser.getRecursivelyObjectsWithTag(
						polyline.getIomObject(), "COORD", "ARC");


				checkControlPointReuseInPolyline(vertices, polyline.getDebugInfo());

				// only continue if there were no control points reused
				if (errors.size() == 0) {
					checkSegmentIntersectionWithDirectPredecessor(
							vertices, maxOverlap, polyline.getDebugInfo());
				}

				// only continue if there were no control points reused and
				// no invalid intersections of segments with their direct predecessors
				if (errors.size() == 0) {
					checkSegmentIntersectionWithAnyPredecessor(
							vertices, polyline.getDebugInfo());
				}
			}
		}
	}
	
	/**
	 * This function looks at a single line string = polyline and checks if two
	 * directly neighbouring segments intersect (i.e. overlap);
	 * 
	 * Implements requirements SLS2, SLS3, SLS4, SLS5 together with
	 * checkSegmentIntersectionWithAnyPredecessor().
	 * 
	 * @param vertices List&lt;IomObject&gt; of type COORD and ARC of a single polyline
	 * @param maxOverlap WITHOUT OVERLAPS &gt; value for this geometry
	 * @param debugInfo Debug information to print in case of errors
	 */
	private void checkSegmentIntersectionWithDirectPredecessor(
			final List<IomObject> vertices,
			final double maxOverlap,
			final String debugInfo) {
	
		List<String> messages = 
				GeometryHelper.getSegmentIntersectionsWithDirectPredecessorMsgs(vertices, maxOverlap, debugInfo);
		
		for (String message: messages) {
			if (message.contains("CIRCULARSTRING")) {
				errors.add("Intersection between two directly connected segments (at least one of them is an arc segment): " + message);
			} else {
				errors.add("Intersection between two directly connected straight segments: " + message);
			}
		}
	}
	
	/**
	 * This function looks at a single line string = polyline and checks if any
	 * segments of it intersect each other;
	 * 
	 * Implements requirements SLS2, SLS3, SLS4, SLS5 together with
	 * checkSegmentIntersectionWithAnyPredecessor(
	 * 
	 * @param vertices List&lt;IomObject&gt; of type COORD and ARC of a single polyline
	 * @param maxOverlap WITHOUT OVERLAPS &gt; value for this geometry
	 * @param debugInfo Debug information to print in case of errors
	 */
	private void checkSegmentIntersectionWithAnyPredecessor(
			final List<IomObject> vertices,
			final String debugInfo) {
	
		List<String> messages =
				GeometryHelper.getSegmentIntersectionsWithAnyPredecessorMsgs(vertices, debugInfo);
	
		for (String message : messages) {
			errors.add("Intersection between two segments of the same line string: " + message);
		}
	}

	/**
	 * Implements requirement SLS1
	 * 
	 * @param vertices
	 * @param modelTopicClassName
	 */
	private void checkControlPointReuseInPolyline(
			final List<IomObject> vertices,
			final String debugInfo) {

		Set<Pair<?>> coordSet = new HashSet<Pair<?>>();

		Pair<?> firstCoords = vertex2Pair(vertices.get(0));

		// iterate over segments
		for (ListIterator<IomObject> it = vertices.listIterator(); it.hasNext(); ) {
			IomObject coord = it.next();
			Pair<?> currentCoords = vertex2Pair(coord);
			if (coordSet.contains(currentCoords) && // coordinate pair was seen before
					(it.hasNext() || // there are more coordinate pairs
							!(currentCoords.equals(firstCoords)))) { // current pair not the same as first pair
				errors.add("Polyline reuses control point with C1=" +
						currentCoords.getEl1() + " and C2=" + 
						currentCoords.getEl2() + " in " + debugInfo);
			} else {
				coordSet.add(currentCoords);
			}
		}
	}
	
	/**
	 * 
	 * @param iomObject
	 * @return
	 * @pre iomObject.getobjecttag() == "COORD" || iomObject.getobjecttag() == "ARC" 
	 */
	private static Pair<?> vertex2Pair(final IomObject iomObject) {
		if (iomObject.getobjecttag() == "COORD" || iomObject.getobjecttag() == "ARC") {
			String c1 = iomObject.getattrvalue("C1");
			String c2 = iomObject.getattrvalue("C2");
			
			return new Pair<Double>(Double.parseDouble(c1), Double.parseDouble(c2));
			
		} else {
			throw new IllegalArgumentException();
		}
	}
}



















