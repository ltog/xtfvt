package ch.htwchur.xtfvt.checks;

import java.util.ArrayList;
import java.util.List;

import ch.interlis.iom.IomObject;
import ch.interlis.iom_j.xtf.XtfReader;
import ch.interlis.iox.*;

/**
 * All checkers that want to use IOX-ILIs XtfReader should extend this class and
 * implement the method handleObjectEvent().
 * 
 * @author ltog
 *
 */
public abstract class CheckerWithXtfReader implements Checker {
	
	protected String path;
	protected List<String> errors = new ArrayList<String>();
	
	public CheckerWithXtfReader(String path) {
		this.path = path;
	}
	
	protected abstract void handleObjectEvent(IomObject iomObject);
	
	public List<String> performValidation() {
		IoxEvent ioxEvent;
		
		XtfReader xtfReader = null;
		try {
			
			xtfReader = new XtfReader(new java.io.File(path));

			// iterate over xtf file
			do {
				ioxEvent = xtfReader.read();

				if (ioxEvent instanceof ObjectEvent) {
					handleObjectEvent(((ObjectEvent)ioxEvent).getIomObject());
				}
			} while (!(ioxEvent instanceof EndTransferEvent));

		} catch (IoxException e) {
			errors.add(e.toString());
		} finally {
			if (xtfReader != null) {
				try {
					xtfReader.close();
				} catch (IoxException e) {
					errors.add(e.toString());
				}
			}		
		}
		
		return errors;
	}
}
