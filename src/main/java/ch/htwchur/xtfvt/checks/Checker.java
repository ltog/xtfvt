package ch.htwchur.xtfvt.checks;

import java.util.List;

public interface Checker {
	
	/**
	 * Perform the validation defined in the check
	 * @return A list of validation errors (if any)
	 */
	List<String> performValidation();
}
