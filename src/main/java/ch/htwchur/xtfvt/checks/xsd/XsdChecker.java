package ch.htwchur.xtfvt.checks.xsd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

import ch.htwchur.xtfvt.checks.Checker;
import ch.htwchur.xtfvt.datastructures.Settings;

/**
 * Performs a validation of a transfer file using an XML Schema (.xsd) file
 * 
 * @author ltog
 *
 */
public class XsdChecker implements Checker{
	
	private Settings settings;
	private File xsdFile;
	private List<String> errors = new ArrayList<String>();
	
	public XsdChecker(final Settings settings, final File xsdFile) {
		this.settings = settings;
		this.xsdFile  = xsdFile;
	}
	
	
	public List<String> performValidation() {
		Source xsdStreamSource = new StreamSource(xsdFile);
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance(
				javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
		
		Schema schema = null;
		try {
			schema = schemaFactory.newSchema(xsdStreamSource);
		} catch (SAXException e) {
			errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
					": couldn't parse the schema from " + xsdStreamSource.toString() +
					"\n" + e.toString());
			return errors;
		}
		
		ValidatorHandler validatorHandler = schema.newValidatorHandler();
		
		org.xml.sax.XMLReader xmlReader = null;
		try {
			xmlReader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e) {
			errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
					": couldn't create XML reader");
			return errors;
		}
		
		xmlReader.setContentHandler(validatorHandler);
		
		try {
			xmlReader.parse(settings.getXtfFile()); // does nothing if parsing is ok
			                                        // otherwise throws an exception
		} catch (IOException e) {
			errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
					": could not access file " + settings.getXtfFile());
		} catch (SAXException e) {
			errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
					": xml not valid in " + settings.getXtfFile());
		}
		
		return errors;
	}
	
}
