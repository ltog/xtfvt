package ch.htwchur.xtfvt.checks.segments;

import java.util.List;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.IomObjectWithInfo;
import ch.htwchur.xtfvt.helpers.IomBrowser;
import ch.interlis.iom.IomObject;

/**
 * Checker for curve segments; Implements requirement CS1
 * @author ltog
 *
 */
public class CurveSegmentChecker extends CheckerWithXtfReader {
	
	public CurveSegmentChecker(final String path) {
		super(path);
	}

	@Override
	protected void handleObjectEvent(IomObject iomObject) {
		
		List<IomObjectWithInfo> segments = IomBrowser.getRecursivelyObjectsWithInfoHavingTag(
				iomObject, "SEGMENTS");
		
		for (IomObjectWithInfo segment : segments) {
			processSegmentsObject(segment.getIomObject(), segment.getDebugInfo());
		}
	}
	
	/**
	 * Process curve segments and check if two consecutive pairs of coordinates
	 * are identical; Implements requirement CS1
	 * 
	 * @pre iomObject.getobjecttag() == "SEGMENTS"
	 * @pre segments need to have at least two dimensions //TODO
	 */
	private void processSegmentsObject(IomObject segment, String debugInfo){
		
		Double c1 = null;
		Double c2 = null;
		Double c1Prev = null;
		Double c2Prev = null;

		// iterate over segments
		for (int j=0; j<segment.getattrvaluecount("segment"); j++) {
			IomObject innerIomObject = segment.getattrobj("segment", j);
			
			String c1String = innerIomObject.getattrvalue("C1");
			String c2String = innerIomObject.getattrvalue("C2");
			
			c1 = Double.parseDouble(c1String);
			c2 = Double.parseDouble(c2String);
			
			if (j>0 && c1.doubleValue() == c1Prev.doubleValue() &&
					c2.doubleValue()==c2Prev.doubleValue()) {
				errors.add("Duplicate C1/C2 pairs in consecutive curve segments; C1=" +
						c1String + " C2=" + c2String + " in " + debugInfo);
			}
			
			c1Prev = c1;
			c2Prev = c2;
		}
	}
	
}
