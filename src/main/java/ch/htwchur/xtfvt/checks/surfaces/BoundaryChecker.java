package ch.htwchur.xtfvt.checks.surfaces;

import java.util.List;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.*;
import ch.htwchur.xtfvt.helpers.GeometryHelper;
import ch.htwchur.xtfvt.helpers.IomBrowser;
import static ch.htwchur.xtfvt.helpers.IomBrowser.*;
import ch.interlis.iom.IomObject;

/**
 * Checks for the self-intersection of boundaries
 * Implements requirements S1, S2, S3, S4 
 * 
 * @author ltog
 *
 */
public class BoundaryChecker extends CheckerWithXtfReader {
	
	private MTCN2MaxOverlap mtcn2MaxOverlap;

	public BoundaryChecker(
			final String path,
			final MTCN2MaxOverlap mtcn2MaxOverlap) {
		
		super(path);
		this.mtcn2MaxOverlap = mtcn2MaxOverlap;
	}

	@Override
	// one objectEvent corresponds to one class instance
	protected void handleObjectEvent(IomObject iomObject) {
		
		final List<IomObjectWithInfo> boundaries =
				getRecursivelyObjectsWithInfoHavingTag(iomObject, "BOUNDARY");
		
		for (IomObjectWithInfo boundary : boundaries) {
			Double maxOverlap = mtcn2MaxOverlap.get(boundary.getModelTopicClassName());
			handleBoundary(boundary, maxOverlap, boundary.getDebugInfo());
		}
	}
	
	private void handleBoundary(
			final IomObjectWithInfo boundary,
			final Double maxOverlap,
			final String debugInfo) {
		
		final List<IomObject> vertices =
				IomBrowser.getRecursivelyObjectsWithTag(boundary.getIomObject(), "COORD", "ARC");
		
		handleSegmentIntersectionsWithDirectPredecessor(vertices, maxOverlap, debugInfo);
		
		if (errors.size() == 0) {
			handleSegmentIntersectionsWithAnyPredecessor(vertices, debugInfo);
		}
	}
	
	private void handleSegmentIntersectionsWithDirectPredecessor(
			final List<IomObject> vertices,
			final double maxOverlap,
			final String debugInfo) {
		
		List<String> messages = 
				GeometryHelper.getSegmentIntersectionsWithDirectPredecessorMsgs(vertices, maxOverlap, debugInfo);
		
		for (String message : messages) {
			errors.add("Intersection between two directly connected segments of the same boundary: " + message);
		}
	}
	
	private void handleSegmentIntersectionsWithAnyPredecessor(
			final List<IomObject> vertices,
			final String debugInfo) {
		
		List<String> messages = 
				GeometryHelper.getSegmentIntersectionsWithAnyPredecessorMsgs(vertices, debugInfo);
		
		for (String message : messages) {
			errors.add("Intersection between two segments of the same boundary: " + message);
		}
	}

}
