package ch.htwchur.xtfvt.checks.surfaces;

import static ch.htwchur.xtfvt.helpers.IomBrowser.getRecursivelyObjectsWithInfoHavingTag;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.LineString;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.IomObjectWithInfo;
import ch.interlis.iom.IomObject;
import ch.htwchur.xtfvt.helpers.GeometryHelper;
import ch.htwchur.xtfvt.helpers.IomBrowser;

public class SurfaceChecker extends CheckerWithXtfReader {

	/**
	 * Check surface for intersections between curve segments of its boundaries;
	 * Implements requirement S6
	 * @param path
	 */
	public SurfaceChecker(final String path) {
		super(path);
	}

	@Override
	protected void handleObjectEvent(IomObject iomObject) {
		final List<IomObjectWithInfo> surfaces =
				getRecursivelyObjectsWithInfoHavingTag(iomObject, "SURFACE");
		
		for (IomObjectWithInfo surface: surfaces) {
			handleSurface(surface.getIomObject(), surface.getDebugInfo());
		}
	}
	
	private void handleSurface(final IomObject surface, final String debugInfo) {
		final List<IomObject> boundaries =
				IomBrowser.getRecursivelyObjectsWithTag(surface, "BOUNDARY");
		final List<ArrayList<LineString>> segments = new ArrayList<ArrayList<LineString>>();
	
		// read all curve segments from all boundaries
		for (IomObject boundary : boundaries) {
			final List<IomObject> verticesOfBoundary =
					IomBrowser.getRecursivelyObjectsWithTag(boundary, "COORD", "ARC");
			segments.add((ArrayList<LineString>)GeometryHelper.vertices2Segments(verticesOfBoundary));
		}
		
		List<LineString> boundary1, boundary2;
	
		// check each segment of each boundary against each segment of each other
		// boundary for intersection
		for (int i=0; i<segments.size()-1; i++) {
			boundary1 = segments.get(i);
			for (int j=i+1; j<segments.size(); j++) {
				boundary2 = segments.get(j);
				
				for (LineString segment1 : boundary1) {
					for (LineString segment2: boundary2) {
						if (GeometryHelper.isReallyIntersecting(segment1, segment2)) {
							errors.add("Intersecting boundary segments " + segment1 +
									" and " + segment2 + " in " + debugInfo);
						}
					}
				}
			}
		}
	}

}
