package ch.htwchur.xtfvt.checks.surfaces;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.*;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.*;
import ch.htwchur.xtfvt.helpers.*;
import ch.interlis.iom.IomObject;
import static ch.htwchur.xtfvt.helpers.IomBrowser.*;

/**
 * Checker for open boundaries; Implements requirement S8.
 * @author ltog
 *
 */
public class OpenBoundaryChecker extends CheckerWithXtfReader {

	public OpenBoundaryChecker(final String path) {
		super(path);
	}

	@Override
	protected void handleObjectEvent(IomObject iomObject) {
		List<IomObjectWithInfo> list = IomBrowser.getRecursivelyObjectsWithInfoHavingTag(
				iomObject, "BOUNDARY");

		for (IomObjectWithInfo iomObjectWithInfo : list) {
			processBoundaryForClosednessCheck(iomObjectWithInfo);
		}
	}

	private void processBoundaryForClosednessCheck(IomObjectWithInfo boundaryWithInfo) {
		final IomObject boundary = boundaryWithInfo.getIomObject();
		final List<IomObject> polylines = IomBrowser.getRecursivelyObjectsWithTag(boundary, "POLYLINE");
		Set<Point> onceOccurredPoints = new HashSet<Point>();
		
		for (IomObject polyline : polylines) {
		
			IomObject segments = null;
			for (int i=0; i<getNumberOfAttributes(polyline); i++) {
				if (polyline.getattrname(i) == "sequence") {
					segments = getInnerObject(polyline, i, 0); // the attribute 'sequence' should only have one inner object (SEGMENTS)
					break;
				}
			}
			
			final int numberOfVertices = segments.getattrvaluecount("segment");
			if (numberOfVertices > 0) {
				Point first = getFirstVertex(segments);
				Point last = getLastVertex(segments);
			
				addToOrRemoveFromSet(onceOccurredPoints, first);
				addToOrRemoveFromSet(onceOccurredPoints, last);
			}
		}
		
		if (onceOccurredPoints.size() > 0) {
			for (Point point : onceOccurredPoints) {
				errors.add("Open boundary at point " + point + " in " + boundaryWithInfo.getDebugInfo());
			}
		}
		
	}
	
	private Point getFirstVertex(IomObject segments) {
		IomObject coord = segments.getattrobj("segment", 0);
		
		return createPointFromCoord(coord);
	}

	private Point getLastVertex(IomObject segments) {
		int nrSegments = segments.getattrvaluecount("segment");

		IomObject coord = segments.getattrobj("segment", nrSegments-1);
		
		return createPointFromCoord(coord);
	}
	
	private Point createPointFromCoord(IomObject coord) {
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		
		final double c1 = Double.parseDouble(coord.getattrvalue("C1"));
		final double c2 = Double.parseDouble(coord.getattrvalue("C2"));
		
		return geometryFactory.createPoint(new Coordinate(c1,c2));
	}
	
	/**
	 * Add an object to a set if its not contained yet. Remove it from the set if
	 * it was already in there.
	 * 
	 * @param set
	 * @param thing
	 */
	private <T> void addToOrRemoveFromSet(Set<T> set, T thing) {
		if (set.contains(thing)) {
			set.remove(thing);
		} else {
			set.add(thing);
		}
	}
}
