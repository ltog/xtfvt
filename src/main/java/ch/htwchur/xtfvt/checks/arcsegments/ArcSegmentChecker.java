package ch.htwchur.xtfvt.checks.arcsegments;

import java.util.List;

import ch.htwchur.xtfvt.checks.CheckerWithXtfReader;
import ch.htwchur.xtfvt.datastructures.*;
import ch.htwchur.xtfvt.helpers.*;
import ch.interlis.iom.IomObject;

/**
 * Checker for circle arc segments; Implements requirement CAS2
 * @author ltog
 *
 */
public class ArcSegmentChecker extends CheckerWithXtfReader {

	public ArcSegmentChecker(final String path) {
		super(path);
	}

	@Override
	protected void handleObjectEvent(IomObject iomObject) {
		List<IomObjectWithInfo> list = IomBrowser.getRecursivelyObjectsWithInfoHavingTag(
				iomObject, "SEGMENTS");
		
		for (IomObjectWithInfo iomObjectWithInfo : list) {
			processSegmentsObject(iomObjectWithInfo);
			//TODO: make sure that segments have at least two dimensions
		}
	}

	/**
	 * Process circle arc segments and check if their diameter can span start and
	 * end segment; Implements requirement CAS2
	 * 
	 * @pre iomObject.getobjecttag() == "SEGMENTS"
	 * @pre segments need to have at least two dimensions
	 */
	private void processSegmentsObject(IomObjectWithInfo iomObjectWithInfo) {
		
		String c1;
		String c2;
		String c1Prev = null;
		String c2Prev = null;
		final IomObject iomObject = iomObjectWithInfo.getIomObject();
		
		// iterate over segments
		for (int j=0; j<iomObject.getattrvaluecount("segment"); j++) {
			IomObject innerIomObject = iomObject.getattrobj("segment", j);
			
			c1 = innerIomObject.getattrvalue("C1");
			c2 = innerIomObject.getattrvalue("C2");
			String rString  = innerIomObject.getattrvalue("R");
			
			// check if radius is given
			if (innerIomObject.getobjecttag() == "ARC" && rString != null && j>0){
				final double r = Double.parseDouble(rString); 
				if (2*r < GeometryHelper.distance(c1, c2, c1Prev, c2Prev)) {
					errors.add("Radius given in circle arc segment is too small; C1=" + 
							c1 + ", C2=" + c2 + ", previous C1=" + c1Prev +
							", previous C2=" + c2Prev + ", R=" + rString + " in " 
							+ iomObjectWithInfo.getDebugInfo());
				}
			}
			
			c1Prev = c1;
			c2Prev = c2;
		}
	}
	
}
