package ch.htwchur.xtfvt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ch.htwchur.xtfvt.checks.*;
import ch.htwchur.xtfvt.checks.arcsegments.*;
import ch.htwchur.xtfvt.checks.segments.*;
import ch.htwchur.xtfvt.checks.simplelinestrings.*;
import ch.htwchur.xtfvt.checks.surfaces.*;
import ch.htwchur.xtfvt.checks.xsd.*;
import ch.htwchur.xtfvt.datastructures.*;
//import ch.htwchur.xtfvt.helpers.IliPrinter;
//import ch.htwchur.xtfvt.helpers.XtfPrinter;
import ch.interlis.ili2c.metamodel.TransferDescription;

public class Xtfvt {

	// information about invalid transfer files will be written from all checkers
	// into this list
	private List<String> errors = new ArrayList<String>();

	// maximum overlap information from all model files
	private MTCN2MaxOverlap mtcn2MaxOverlap = new MTCN2MaxOverlap();

	// settings given by (simulated or real) command line parameters
	private Settings settings;
	
	public Xtfvt(Settings settings) {
		this.settings = settings;
	}

	public XtfValidationResultWithErrorList validate() throws IOException {

		// create a temporary file to store the XML Schema information
		File xsdFile = File.createTempFile("xtfvt", null);
		xsdFile.deleteOnExit(); // delete file after program has finished
		
		Compiler compiler = new Compiler(settings, xsdFile);
		errors.addAll(compiler.runCompiler());
		if (errors.size() > 0) {
			return new XtfValidationResultWithErrorList(
					// if the compiler fails to read/compile model files, we don't know 
					// whether the transfer file is valid
					XtfValidationResult.UNDECIDED, errors);
		}
		
		TransferDescription transferDescription = compiler.getTransferDescription();
		if (transferDescription == null) {
			errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
					": transferDescription is null; Possible reason: inaccessible model file(s)");
			return new XtfValidationResultWithErrorList(
					XtfValidationResult.UNDECIDED, errors);
		}
		
		// read and print ili file information (for debugging)
//		IliPrinter iliPrinter = new IliPrinter((TransferDescription)transferDescription);
		// read and print xtf file information (for debugging)
//		XtfPrinter xtfPrinter = new XtfPrinter(settings.getXtfFile());
		
		mtcn2MaxOverlap.init(transferDescription);
	
		// create a list of checkers to call them afterwards
		// (entries on top are called first)
		List<Checker> checkers = new ArrayList<Checker>();
		checkers.add(new XsdChecker(settings, xsdFile));
		checkers.add(new CurveSegmentChecker(settings.getXtfFile()));
		checkers.add(new ArcSegmentChecker(settings.getXtfFile()));
		checkers.add(new OpenBoundaryChecker(settings.getXtfFile()));
		checkers.add(new SimpleLineStringChecker(settings.getXtfFile(), mtcn2MaxOverlap));
		checkers.add(new BoundaryChecker(settings.getXtfFile(), mtcn2MaxOverlap));
		checkers.add(new SurfaceChecker(settings.getXtfFile()));
	
		for (Iterator<Checker> it = checkers.iterator();
				it.hasNext() && errors.size()==0; ) {
			Checker checker = it.next();
			errors.addAll(checker.performValidation());
		}
		
		if (errors.isEmpty()) {
			return new XtfValidationResultWithErrorList(XtfValidationResult.PASS, errors);
		} else {
			return new XtfValidationResultWithErrorList(XtfValidationResult.FAIL, errors);
		}

		
	}
}
