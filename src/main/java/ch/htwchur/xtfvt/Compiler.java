package ch.htwchur.xtfvt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ch.htwchur.xtfvt.datastructures.Settings;
import ch.htwchur.xtfvt.helpers.IomXtfUtilityHelper;
import ch.interlis.ili2c.*;
import ch.interlis.ili2c.config.*;
import ch.interlis.ili2c.metamodel.TransferDescription;
import ch.interlis.ilirepository.IliManager;
import ch.interlis.iom_j.xtf.XtfUtility;
import ch.interlis.iox.IoxException;

/**
 * This class uses the INTERLIS 2 compiler ili2c.
 * 
 * @author ltog
 *
 */
public class Compiler {
	
	private TransferDescription transferDescription = null;
	private Settings settings;
	private File xsdFile;
	private List<String> errors = new ArrayList<String>();

	public Compiler(Settings settings, File xsdFile) {
		this.settings = settings;
		this.xsdFile = xsdFile;
	}

	/**
	 * @pre runCompiler() must be run before and must not have returned any errors
	 * 
	 * @return The transfer description created from the ili files given in settings
	 * in the constructor of this class.
	 */
	public TransferDescription getTransferDescription() {
		return transferDescription;
	}
	
	/**
	 * Create XSD schema file and transfer description (unfortunately the
	 * creation of those two can't be seperated)
	 * 
	 * @param settings The settings for configuring xtfvt
	 * @param xsdFile The resulting XSD file will be written there
	 * @return A TransferDescription which represents the encountered model(s)
	 */
	public List<String> runCompiler(){
		ArrayList<String> models = null; // needs to be ArrayList not the generic type List, as it is used as argument to IliManager.getConfig()
		IliManager iliManager = new IliManager(); // enables cached access to model repositories
		Configuration configuration = null;
		
		if (settings.getIliFiles().isEmpty()) { // no ILI files were given
			try {
				models = IomXtfUtilityHelper.collection2ArrayList(XtfUtility.getModels(new File (settings.getXtfFile())));
			} catch (IoxException e) {
				errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
						": could not detect a model in file " + settings.getXtfFile() +
						" Exception: " + e.toString());
				return errors;
			}

			if (models.size() < 1) {
				errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
						": could not detect a model in file " + settings.getXtfFile() +
						" models.size() < 1");
				return errors;
			}
			
			List<String> repositories = new ArrayList<String>();
			repositories.add(new File(settings.getXtfFile()).getParentFile().getAbsolutePath());
			repositories.addAll(settings.getRepositories());
			
			iliManager.setRepositories(repositories.toArray(new String[0]));

			try {
				configuration = iliManager.getConfig(models, 0.0);
			} catch (Ili2cException e) {
				errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
						": exception in iliManager.getConfig(): " + e.toString());
				return errors;
			}
		} else { // ILI files were given
			try {
				configuration = iliManager.getConfigWithFiles((ArrayList<String>)settings.getIliFiles());
			} catch (Ili2cException e) {
				errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
						": exception in getConfigWithFiles(): " + e.toString());
				return errors;
			}
		}
		
		configuration.setOutputFile(xsdFile.toString());
		configuration.setOutputKind(GenerateOutputKind.XMLSCHEMA);
		
		try {
			transferDescription = Ili2c.runCompiler(configuration);
		} catch (Ili2cFailure e) {
				errors.add("ERROR in xtfvt's class " + this.getClass().getSimpleName() +
						": runCompiler() failed with exception " + e.toString());
				return errors;
		}
		
		return errors;
	}
}
