package ch.htwchur.xtfvt.datastructures;

import java.util.List;

public class XtfValidationResultWithErrorList {
	
	private XtfValidationResult xtfValidationResult;
	private List<String> errors;
	
	public XtfValidationResultWithErrorList(
			XtfValidationResult xtfValidationResult,
			List<String> errors) {
		
		this.xtfValidationResult = xtfValidationResult;
		this.errors              = errors;
	}

	public XtfValidationResult getXtfValidationResult() {
		return xtfValidationResult;
	}

	public List<String> getErrors() {
		return errors;
	}
	
}
