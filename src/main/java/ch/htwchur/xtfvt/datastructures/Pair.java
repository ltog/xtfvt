package ch.htwchur.xtfvt.datastructures;

/**
 * A generic container class for two objects of the same type.
 * 
 * @author ltog
 *
 * @param <T>
 */
public class Pair<T> {
	
	private T el1;
	private T el2;
	
	public Pair(T  el1, T  el2) {
		this.el1 = el1;
		this.el2 = el2;
	}

	public T  getEl1() {
		return el1;
	}

	public T  getEl2() {
		return el2;
	}

	@Override
	public String toString() {
		return "Pair [el1=" + el1 + ", el2=" + el2 + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((el1 == null) ? 0 : el1.hashCode());
		result = prime * result + ((el2 == null) ? 0 : el2.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair<T> other = (Pair<T>) obj;
		if (el1 == null) {
			if (other.el1 != null)
				return false;
		} else if (!el1.equals(other.el1))
			return false;
		if (el2 == null) {
			if (other.el2 != null)
				return false;
		} else if (!el2.equals(other.el2))
			return false;
		return true;
	}
}
