package ch.htwchur.xtfvt.datastructures;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.*; // see http://jcommander.org

/**
 * Command line options to configure xtfvt. 
 * @author ltog
 *
 */
public class Settings {

	@Parameter(names = "-i", description = ".ili file path")
	private List<String> iliFiles = new ArrayList<String>();
	
	@Parameter(names = "-x", description = ".xtf file path")
	private String xtfFile = null;
	
	@Parameter(names = "-r", description = "model repositories")
	private List<String> repositories = new ArrayList<String>();
	
	public List<String> getIliFiles() {
		return iliFiles;
	}
	
	public String getXtfFile() {
		return xtfFile;
	}
	
	public List<String> getRepositories() {
		return repositories;
	}
	
}
