package ch.htwchur.xtfvt.datastructures;

/**
 * The result of the validation of an xtf file. If the validation can not be performed,
 * e.g. because of unreadable model files, the result is UNDECIDED.
 * 
 * @author ltog
 *
 */
public enum XtfValidationResult {
	PASS, FAIL, UNDECIDED;
}