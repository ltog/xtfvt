package ch.htwchur.xtfvt.datastructures;

import ch.interlis.iom.IomObject;

/**
 * This class complements an instance of IomObject with further information, which
 * will be used for example for printing debug information.
 * 
 * @author ltog
 *
 */
public class IomObjectWithInfo {
	
	private final IomObject iomObject;
	private final String modelTopicClass;
	private final String name;
	private final String oid;
	private final int line;
	private final int column;

	public IomObjectWithInfo (
			final IomObject iomObject,
			final String modelTopicClass,
			final String name,
			final String oid,
			final int line,
			final int column) {
		
		this.iomObject = iomObject;
		this.modelTopicClass = modelTopicClass;
		this.name = name;
		this.oid = oid;
		this.line = line;
		this.column = column;
	}

	public IomObject getIomObject() {
		return iomObject;
	}

	public String getModelTopicClass() {
		return modelTopicClass;
	}
	
	public String getName() {
		return name;
	}
	
	public String getModelTopicClassName() {
		return modelTopicClass + "." + name;
	}
	
	public String getOid() {
		return oid;
	}

	/**
	 * 
	 * @return The line number of this object in the transfer file
	 */
	public int getLine() {
		return line;
	}

	/**
	 * 
	 * @return The column number of this object in the transfer file
	 */
	public int getColumn() {
		return column;
	}
	
	public String getDebugInfo() {
		return getModelTopicClassName() + " OID=" + oid + " near line " + line + " column " + column;
	}
}
