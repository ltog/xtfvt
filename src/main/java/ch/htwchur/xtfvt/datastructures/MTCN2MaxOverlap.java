package ch.htwchur.xtfvt.datastructures;

import java.util.HashMap;
import java.util.List;

import ch.htwchur.xtfvt.helpers.IliHelper;
import ch.interlis.ili2c.metamodel.*;
/**
 * This class represents a mapping from Model.Topic.Class.Name to maximum overlap
 * values (WITHOUT OVERLAPS > ... in the model file)
 * 
 * @author ltog
 *
 */
public class MTCN2MaxOverlap {

	private HashMap<String, Double> mtcn2MaxOverlap = new HashMap<String, Double>();
	
	public MTCN2MaxOverlap() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Read the model data and store a mapping from model.topic.class.name to
	 * maximum overlap values.
	 */
	public void init(TransferDescription transferDescription) {
		List<Element> localAttributes = IliHelper.getRecursivelyElementsOfType(
				transferDescription, LocalAttribute.class, true);
		
		for (Element element : localAttributes) {
			Type type = ((AttributeDef)element).getDomain();
			if (type instanceof LineType && ((LineType)type).getMaxOverlap() != null) {
				final String mtcn = IliHelper.getModelTopicClassName(element);
				if (mtcn != null) {
					mtcn2MaxOverlap.put(
							mtcn,
							Double.parseDouble(((LineType)type).getMaxOverlap().toString()));
				}
			}
		}
	}
	
	public Double get(String modelTopicClassName) {
		return mtcn2MaxOverlap.get(modelTopicClassName);
	}

}
