package ch.htwchur.xtfvt;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ch.htwchur.xtfvt.datastructures.*;

import com.beust.jcommander.*;


public class XtfvtMain {
	public XtfvtMain() {
		
	}
	
	public static void main(String[] args) throws IOException {
		
		Settings settings = new Settings();
		new JCommander(settings, args);
		
		Xtfvt xtfvt = new Xtfvt(settings);
	
		XtfValidationResultWithErrorList result = xtfvt.validate();
		
		System.out.println("Result of validation for file "
				+ (new File(settings.getXtfFile())).getAbsolutePath() 
				+ " is: " + result.getXtfValidationResult());
	
		List<String> errors = result.getErrors();
		if (errors.size() > 0) {
			System.out.println("Error messages are:");
			for (String error : errors) {
				System.out.println(error);
			}
		}
	}
}
