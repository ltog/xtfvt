package ch.htwchur.xtfvt.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.htwchur.xtfvt.datastructures.IomObjectWithInfo;
import ch.interlis.iom.IomObject;

/**
 * Helper functions to more comfortably read content from xtf files.
 * @author ltog
 *
 */
public class IomBrowser {
	
	/**
	 * 
	 * @param previousModelTopicClass The Model.Topic.Class string detected in outer
	 * executions of this recursive function (used in the assumption that the
	 * ModelTopicClass can not be detected in inner funtion calls)
	 * @param iomObject Object that will be recursively traversed
	 * @param wantedObjectTags Object tag (type of object) we are looking for
	 * @return A List of IomObjects matching the wanted type of object and their
	 * corresponding Model.Topic.Class strings
	 * 
	 * @post: Objects in return List are guaranteed to be != null
	 */
	public static List<IomObjectWithInfo> getRecursivelyObjectsWithInfoHavingTag(
			final String previousModelTopicClass,
			final String previousClassAttributeName,
			final String previousOid,
			final int previousLine,
			final int previousColumn,
			final IomObject iomObject,
			final String... wantedObjectTags) {
		
		return getRecursivelyObjects(previousModelTopicClass,
				previousClassAttributeName, previousOid, previousLine, previousColumn,
				iomObject, false, wantedObjectTags);
	}
	
	
	public static List<IomObject> getRecursivelyObjectsWithTag(
			final IomObject iomObject,
			final String... wantedObjectTags) {

		List<IomObjectWithInfo> iomObjectsWithInfo =
				getRecursivelyObjectsWithInfoHavingTag(iomObject, wantedObjectTags);
		List<IomObject> iomObjects = new ArrayList<IomObject>();
		
		for (IomObjectWithInfo iomObjectWithInfo : iomObjectsWithInfo) {
			iomObjects.add(iomObjectWithInfo.getIomObject());
		}
		
		return iomObjects;
	}
	
	public static List<IomObjectWithInfo> getRecursivelyObjectsWithInfoHavingTag(
			final IomObject iomObject,
			final String... wantedObjectTags) {
		
		return getRecursivelyObjectsWithInfoHavingTag(null, null, null, 0, 0, iomObject,
				wantedObjectTags);
	}
	
	public static List<IomObjectWithInfo> getRecursivelyObjects(
		final String previousModelTopicClass,
		final String previousClassAttributeName,
		final String previousOid,
		final int previousLine,
		final int previousColumn,
		final IomObject iomObject,
		final boolean childMustMeetCondition,
		final String... wantedObjectTags) {
		
		List<IomObjectWithInfo> list = new ArrayList<IomObjectWithInfo>();
		
		// Use previous... and current... so method arguments can be made final
		String currentModelTopicClass = previousModelTopicClass;
		String currentClassAttributeName = previousClassAttributeName;
		String currentOid = previousOid;
		int currentLine = previousLine;
		int currentColumn = previousColumn;
		
		if (iomObject.getobjectoid() != null) {
			currentModelTopicClass = iomObject.getobjecttag();
			currentOid = iomObject.getobjectoid();
		}
		
		if (iomObject.getobjectline() != 0) {
			currentLine = iomObject.getobjectline();
		}
		
		if (iomObject.getobjectcol() != 0) {
			currentColumn = iomObject.getobjectcol();
		}
		

		// the current object is one of those we are looking for
		if ((childMustMeetCondition &&
				objectHasDirectChildHavingTag(iomObject, wantedObjectTags)) ||
				(!(childMustMeetCondition) &&
				Arrays.asList(wantedObjectTags).contains(iomObject.getobjecttag()))) {
			
			list.add(new IomObjectWithInfo(iomObject, currentModelTopicClass,
					currentClassAttributeName, currentOid, currentLine, currentColumn));
			
		} else {  // search for inner objects
			// iterate over attributes(=properties)
			for (int i=0; i<getNumberOfAttributes(iomObject); i++) {
				
				if (iomObject.getobjectoid() != null) {
					currentClassAttributeName = iomObject.getattrname(i);
				}
				
				if (isStructuredAttribute(iomObject, i)) {
					// iterate over inner objects of this attribute
					for (int j=0; j<getNumberOfInnerObjects(iomObject, i); j++) {
						IomObject innerIomObject = getInnerObject(iomObject, i, j);
						if (innerIomObject != null) {
							// do recursion
							list.addAll(getRecursivelyObjects(currentModelTopicClass,
									currentClassAttributeName, currentOid, currentLine,
									currentColumn, innerIomObject,
									childMustMeetCondition, wantedObjectTags));
						}
					}
				} 
			}
		}
		
		return list;
	}
	
	public static boolean objectHasDirectChildHavingTag(
			final IomObject iomObject, final String... wantedObjectTags) {

		return objectHasChildHavingTag(iomObject, false, wantedObjectTags);
	}
	
	public static boolean objectHasAnyDescendantHavingTag(
			final IomObject iomObject, final String... wantedObjectTags) {
		
		return objectHasChildHavingTag(iomObject, true, wantedObjectTags);
	}
	
	public static boolean objectHasChildHavingTag(
			final IomObject iomObject,
			final boolean isAnyDescendantOk,
			final String... wantedObjectTags) {
		
		for (int i=0; i<getNumberOfAttributes(iomObject); i++) {
			if (isStructuredAttribute(iomObject, i)) {
				// iterate over values (objects) of this attribute
				for (int j=0; j<getNumberOfInnerObjects(iomObject, i); j++) {
					IomObject innerIomObject = getInnerObject(iomObject, i, j);
					if (innerIomObject != null) {
						if (((!isAnyDescendantOk) &&
								Arrays.asList(wantedObjectTags).contains(innerIomObject.getobjecttag())) ||
								(isAnyDescendantOk &&
								objectHasChildHavingTag(innerIomObject, isAnyDescendantOk, wantedObjectTags))) {
							
							return true;
						}
					}
				}
			} 
		}
		
		return false;
	}
	
	public static boolean isStructuredAttribute(
			final IomObject iomObject,
			final String attributeName) {
		
		String attributeValue = iomObject.getattrvalue(attributeName);
		if (attributeValue == null) { // structured attribute
			return true;
		} else { // primitive attribute
			return false;
		}
	}
	
	public static boolean isPrimitiveAttribute(
			final IomObject iomObject,
			final String attributeName) {
		
		return !isStructuredAttribute(iomObject, attributeName);
	}

	public static boolean isStructuredAttribute(
			final IomObject iomObject,
			final int attributeIndex) {
		
		return isStructuredAttribute(iomObject,
				iomObject.getattrname(attributeIndex));
	}
	
	public static boolean isPrimitiveAttribute(
			final IomObject iomObject,
			final int attributeIndex) {
		
		return isPrimitiveAttribute(iomObject, iomObject.getattrname(attributeIndex));
	}
	
	/**
	 * Get model, topic and class of this object, separated by periods.
	 * 
	 * Must NOT be called for subobjects of class objects.
	 * 
	 * @pre iomObject is a class object
	 * @param iomObject
	 * @return
	 */
	public static String getModelTopicClass(final IomObject iomObject) {
		return iomObject.getobjecttag();
	}
	
	/**
	 * Get model, topic, class and name (separated by periods) of an attribute
	 * of an object.
	 * 
	 * Must NOT be called for subobjects of class objects.
	 * 
	 * @pre iomObject is a class object
	 * @param iomObject
	 * @param attributeIndex
	 * @return
	 */
	public static String getModelTopicClassName(
			final IomObject iomObject,
			final int attributeIndex) {
		
		return iomObject.getobjecttag() + "." + iomObject.getattrname(attributeIndex);
	}

	/**
	 * 
	 * @param iomObject
	 * @param attributeIndex
	 * @param innerObjectIndex
	 * @return
	 */
	public static IomObject getInnerObject(
			final IomObject iomObject,
			final int attributeIndex,
			final int innerObjectIndex ) {
		
		return iomObject.getattrobj(iomObject.getattrname(attributeIndex),
				innerObjectIndex);
	}
	
	public static int getNumberOfInnerObjects(
			final IomObject iomObject,
			final int attributeIndex) {
		
		if (isPrimitiveAttribute(iomObject, attributeIndex)) {
			return 0;
		} else {
			return iomObject.getattrvaluecount(iomObject.getattrname(attributeIndex));
		}
	}
	
	public static int getNumberOfAttributes(final IomObject iomObject) {
		return iomObject.getattrcount();
	}
	
}
















