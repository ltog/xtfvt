package ch.htwchur.xtfvt.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.geotools.geometry.jts.CircularString;
import org.geotools.geometry.jts.CurvedGeometryFactory;
import org.geotools.geometry.jts.JTSFactoryFinder;

import ch.interlis.iom.IomObject;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.PackedCoordinateSequence;
import com.vividsolutions.jts.linearref.LengthLocationMap;
import com.vividsolutions.jts.linearref.LinearLocation;
import com.vividsolutions.jts.linearref.LocationIndexedLine;

/**
 * A class containing various helper functions for geometrical operations.
 * 
 * @author ltog
 *
 */
public class GeometryHelper {

	private static final GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
	private static final CurvedGeometryFactory curvedGeometryFactory =
			new CurvedGeometryFactory(geometryFactory,0);

	/**
	 * Create a LineString from two vertices that are either of type ARC or type
	 * COORD (they don't have to be of the same type)
	 * 
	 * @param previousVertex
	 * @param currentVertex
	 * @return
	 * @pre iomObject.getobjecttag() == "COORD" || iomObject.getobjecttag() == "ARC"
	 */
	public static LineString vertices2Segment(
			final IomObject previousVertex,
			final IomObject currentVertex) {
		
		// check types of IomObjects
		if ((previousVertex.getobjecttag() == "COORD" || previousVertex.getobjecttag() == "ARC") &&
				(currentVertex.getobjecttag() == "COORD" || currentVertex.getobjecttag() == "ARC")) {
			
			final double previousC1 = Double.parseDouble(previousVertex.getattrvalue("C1"));
			final double previousC2 = Double.parseDouble(previousVertex.getattrvalue("C2"));
			final double currentC1 = Double.parseDouble(currentVertex.getattrvalue("C1"));
			final double currentC2 = Double.parseDouble(currentVertex.getattrvalue("C2"));
		
			// create straight segment
			if (currentVertex.getobjecttag() == "COORD") {
				Coordinate[] lineStringCoords  = new Coordinate[] {
						new Coordinate(previousC1, previousC2),
						new Coordinate(currentC1, currentC2) };
				
				return geometryFactory.createLineString(lineStringCoords);
			} else { // create arc segment
				double a1 = Double.parseDouble(currentVertex.getattrvalue("A1"));
				double a2 = Double.parseDouble(currentVertex.getattrvalue("A2"));
				
				PackedCoordinateSequence circularStringCoords;
				String radiusAsString = currentVertex.getattrvalue("R");
				
				// we have an exact radius value, so we overwrite a1,a2
				if (radiusAsString != null) {
					final double radius = Double.parseDouble(radiusAsString);
					Point pointOnArc = getExactPointOnArc(
							previousC1, previousC2, a1, a2, radius, currentC1,
							currentC2, geometryFactory, curvedGeometryFactory);
					a1 = pointOnArc.getX();
					a2 = pointOnArc.getY();
				}
				
				// assemble coordinates of the arc segment
				circularStringCoords = 
						new PackedCoordinateSequence.Double(new double[]{
							previousC1, previousC2, a1, a2, currentC1, currentC2	
						}, 2);
				
				return curvedGeometryFactory.createCurvedGeometry(circularStringCoords);
			}
			
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Convert vertices to JTS line strings
	 * 
	 * @param vertices
	 * @return
	 */
	public static List<LineString> vertices2Segments(
			final List<IomObject> vertices) {
		
		if (vertices.size() < 2) {
			return null;
		}
		
		List<LineString> segments = new ArrayList<LineString>();
		
		final ListIterator<IomObject> it = vertices.listIterator();
		
		IomObject previousVertex = it.next();
		IomObject currentVertex  = null;
		while (it.hasNext()) {
			currentVertex = it.next();
			segments.add(vertices2Segment(previousVertex, currentVertex));
			previousVertex = currentVertex;
		}
	
		return segments;
	}

	/**
	 * Get the point lying on the arc. It is similar in position to (A1,A2) but
	 * calculated more precisely using an exact radius value.
	 * 
	 * @param previousC1
	 * @param previousC2
	 * @param a1
	 * @param a2
	 * @param r
	 * @param currentC1
	 * @param currentC2
	 * @param geometryFactory
	 * @param curvedGeometryFactory
	 * 
	 * @pre distance between arc endpoints <= 2*abs(r)
	 * @pre distance between arc endpoints > 0
	 * 
	 * @return
	 */
	private static Point getExactPointOnArc(
			final double previousC1,
			final double previousC2,
			final double a1,
			final double a2,
			final double r,
			final double currentC1,
			final double currentC2,
			final GeometryFactory geometryFactory,
			final CurvedGeometryFactory curvedGeometryFactory) {
		
		final double x1 = previousC1;
		final double y1 = previousC2;
		final double x2 = currentC1;
		final double y2 = currentC2;
		final double xa = a1;
		final double ya = a2;
		
		// directional vector from start to end of circle arc segment
		final double x12 = x2-x1;
		final double y12 = y2-y1;
		
		// distance between start and end of circle arc segment (length of chord)
		final double d = StrictMath.hypot(x12, y12);
	
		// check pre-conditions
		if (r < d/2) {
			throw new IllegalArgumentException("r is too small");
		}
		if (d == 0) {
			throw new IllegalArgumentException("distance between arc endpoints is 0");
		}
		
		// distance from connection line of start and end of segment to candidate
		// points (pythagorean theorem)
		final double h = StrictMath.sqrt(r*r - d*d/4); 
		
		// middle point between start and end of circle arc segment
		final double xm = (x1+x2)/2;
		final double ym = (y1+y2)/2;
		
		// unit directional vector from middle point between start and end of 
		// circle arc segment to one of the centre candidate points
		final double xDir =  y12/d;
		final double yDir = -x12/d;
		
		// is the unit direction vector pointing in direction of clockwise arcs?
		// (lying on the LEFT of the connectiom from start to end of the arc segment)
		boolean isDirPointingToCw;
		if (isLeftOf(x1, y1, x2, y2, x1+xDir, y1+yDir)) {
			isDirPointingToCw = true;
		} else {
			isDirPointingToCw = false;
		}
	
		// change direction of the unit direction vector depending on whether we
		// have a CW or CCW arc and whether the unit direction vector points towards
		// or away from the correct direction
		int factor = 1;
		if (r < 0) { // counter-clockwise (INTERLIS reference manual 3.3.11.12)
			factor *= -1;
		}
		if (isDirPointingToCw == false) {
			factor *= -1;
		}
		
		// create candidate points that lie on one of the two possible arcs and
		// in a line perpendicular to the arc's chord 
		final double xP1 = xm + factor*xDir*(StrictMath.abs(r)-h);
		final double yP1 = ym + factor*yDir*(StrictMath.abs(r)-h);
		final double xP2 = xm + factor*xDir*(StrictMath.abs(r)+h);
		final double yP2 = ym + factor*yDir*(StrictMath.abs(r)+h);
		
		final Point p1 = geometryFactory.createPoint(new Coordinate(xP1, yP1));
		final Point p2 = geometryFactory.createPoint(new Coordinate(xP2, yP2));
		
		final Point a = geometryFactory.createPoint(new Coordinate(xa, ya));
		
		return (a.distance(p1) < a.distance(p2) ? p1 : p2);
	}

	/**
	 * Checks whether point P lies left or right of the line going through points
	 * A and B. Viewing direction is from A to B.
	 * 
	 * Source: http://stackoverflow.com/a/1560510
	 * 
	 * @param xa
	 * @param ya
	 * @param xb
	 * @param yb
	 * @param xp
	 * @param yp
	 * @return
	 */
	public static boolean isLeftOf(
			final double xa,
			final double ya,
			final double xb,
			final double yb,
			final double xp,
			final double yp) {
		return (((xb-xa)*(yp-ya))-((yb-ya)*(xp-xa))>0) ? true : false;
	}
	
	/**
	 * Returns the angle between 3 points. The angle is defined as the smaller
	 * angle between point1 to point2 each connected to the center in (xc,yc).
	 * 
	 * The calculation returns arccos( V1*V2/(|V1|*|V2|) )
	 *
	 * @param x1
	 * @param y1
	 * @param xc x coordinate of the center
	 * @param yc y coordinate of the center
	 * @param x2
	 * @param y2
	 * @return Angle in rad
	 */
	public static double getAngle(
			final double x1,
			final double y1,
			final double xc,
			final double yc,
			final double x2,
			final double y2) {
		return StrictMath.acos((x1*x2 + y1*y2)/
				(StrictMath.hypot(x1, y1)*StrictMath.hypot(x2, y2)));
	}
	
	public static double distance(
			final String c1,
			final String c2,
			final String c1Prev,
			final String c2Prev) {
		
		final double d1 = Double.parseDouble(c1)-Double.parseDouble(c1Prev);
		final double d2 = Double.parseDouble(c2)-Double.parseDouble(c2Prev); 
		
		return StrictMath.hypot(d1, d2);
	}
	
	/**
	 * 
	 * @param ls1
	 * @param ls2
	 * @return True if the two LineStrings have more points in common than their
	 * start/end points
	 */
	public static boolean isReallyIntersecting(LineString ls1, LineString ls2) {
		Geometry intersection = getRealIntersection(ls1, ls2);
		
		if (intersection.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Given two connected segments with a common start/end point but no other
	 * common points, JTS will recognize them as intersecting, even if in a sense
	 * they are not. This function takes into account the start/end points of the
	 * segments, remove them from the set of possible intersection points and
	 * then checks if the segments intersect.  
	 * 
	 * @param ls1
	 * @param ls2
	 * @return
	 */
	public static Geometry getRealIntersection(LineString ls1, LineString ls2) {
		Geometry intersection = ls1.intersection(ls2);
		
		intersection = intersection.difference(ls1.getStartPoint());
		intersection = intersection.difference(ls1.getEndPoint());
		intersection = intersection.difference(ls2.getStartPoint());
		intersection = intersection.difference(ls2.getEndPoint());
		
		return intersection;
	}

	/**
	 * Calculate the sagitta given two LineStrings
	 * 
	 * @param ls1
	 * @param ls2
	 * @return
	 * 
	 * @pre The end point of ls1 must be the start point of ls2
	 */
	public static double getSagitta(LineString ls1, LineString ls2) {
		
		// create new (shorter) linestrings from start/end to intersection point 
		final Point intersectionPoint = (Point) getRealIntersection(ls1, ls2);
	
		final LocationIndexedLine lil1 = new LocationIndexedLine(ls1);
		final LocationIndexedLine lil2 = new LocationIndexedLine(ls2);
		
		final LinearLocation start1 = lil1.indexOf(intersectionPoint.getCoordinate());
		final LinearLocation end1 = lil1.getEndIndex();
		
		final LinearLocation start2 = lil2.getStartIndex();
		final LinearLocation end2 = lil2.indexOf(intersectionPoint.getCoordinate());
		
		final LineString shortenedLineString1 = (LineString) lil1.extractLine(start1, end1);
		final LineString shortenedLineString2 = (LineString) lil2.extractLine(start2, end2);
		
		final Point m1 = getMiddleOfLineString(shortenedLineString1);
		final Point m2 = getMiddleOfLineString(shortenedLineString2);
		
		return m1.distance(m2);
	}
	
	/**
	 * "Walk" along the LineString and return the point after half the total distance
	 * 
	 * @param ls
	 * @return
	 */
	private static Point getMiddleOfLineString(final LineString ls) {
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		final LengthLocationMap lengthLocationMap = new LengthLocationMap(ls);
		final LinearLocation linearLocation = lengthLocationMap.getLocation(ls.getLength()/2);
		final Coordinate coordinate = linearLocation.getCoordinate(ls);
		
		return geometryFactory.createPoint(coordinate);
	}

	/**
	 * Find intersections between directly connected curve segments.
	 * 
	 * @param vertices A list of vertices of type "COORD" or "ARC"
	 * @param maxOverlap The maximum allowed tolerance (WITHOUT OVERLAPS) for overlaps
	 * when a circle arc is involved in an intersection
	 * @param debugInfo Debug information that will be included in a message
	 * @return A list of elements involved in the intersection
	 */
	public static List<String> getSegmentIntersectionsWithDirectPredecessorMsgs(
			final List<IomObject> vertices,
			double maxOverlap,
			final String debugInfo) {
		
		final List<String> messages = new ArrayList<String>();

		// Intersection needs to happen between two directly connected segments.
		// 2 segments = 3 vertices
		if (vertices.size() < 3) {
			return messages;
		}
		
		final List<LineString> segments = vertices2Segments(vertices);
		final ListIterator<LineString> it = segments.listIterator();
		LineString previousSegment = it.next();
		LineString currentSegment  = null;
		
		// iterate over segments
		while (it.hasNext()) {
			currentSegment  = it.next();
			
			if (isReallyIntersecting(previousSegment, currentSegment)) {
				
				// intersection involves an arc segment
				if (previousSegment instanceof CircularString ||
						currentSegment instanceof CircularString) {
					
					final double sagitta = getSagitta(previousSegment, currentSegment);
					if (sagitta > maxOverlap) {
						messages.add(previousSegment + " and " + currentSegment
								+ ", maximum allowed overlap=" + maxOverlap
								+ ", actual overlap= " + sagitta + " in " + debugInfo);
					}
				} else { // intersection is between straight segments
					messages.add(previousSegment + " and " + currentSegment + " in " + debugInfo);
				}
			}
			previousSegment = currentSegment;
		}
		
		return messages;
	}

	/**
	 * Find intersections between curve segments. There is no tolerance value,
	 * every intersection will be reported.
	 * 
	 * @param vertices A list of vertices of type "COORD" or "ARC"
	 * @param debugInfo Debug information that will be included in a message
	 * @return A list of elements involved in the intersection
	 */
	public static List<String> getSegmentIntersectionsWithAnyPredecessorMsgs(
			final List<IomObject> vertices,
			final String debugInfo) {
		final List<String> messages = new ArrayList<String>();
		
		// there must be at least 2 segments = 3 vertices
		if (vertices.size() < 3) {
			return messages;
		}
		
		final List<LineString> segments = vertices2Segments(vertices);
	
		LineString segment1;
		LineString segment2;
		
		for (int i=0; i<segments.size()-1; i++) {
			for (int j=i+1; j<segments.size(); j++) {
				segment1 = segments.get(i);
				segment2 = segments.get(j);
				if (isReallyIntersecting(segment1, segment2)) {
					messages.add(segment1 + " and " + segment2 + " in " + debugInfo);
				}
			}
		}
		return messages;
	}
	
}
