package ch.htwchur.xtfvt.helpers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ch.interlis.ili2c.metamodel.*;

public class IliHelper {

	public static List<Element> getRecursivelyElementsOfType(
			final Element element,
			final Class<?> type,
			final boolean skipPredefined) {
		
		List<Element> list = new ArrayList<Element>();
		
		if (element instanceof PredefinedModel && skipPredefined) {
			return list; // return empty list
		}
		
		if (element instanceof Container<?>) { // element is a composite
			Iterator<?> it = ((Container<?>)element).iterator();
			while (it.hasNext()) {
				Element innerElement = (Element) it.next();
				// do recursion
				list.addAll(getRecursivelyElementsOfType(innerElement, type,
						skipPredefined));
			}
		// current element is from wanted type
		} else if (type.isAssignableFrom(element.getClass())) { 
			list.add(element);
		}
		return list;
	}
	
	public static String getModelTopicClassName(final Element element) {
		final String mtc = getModelTopicClass(element);
		if (mtc != null) {
			return getModelTopicClass(element) + "." + element.getScopedName(null); // TODO: better use getName() ?
		} else {
			return null;
		}
	}
	
	public static String getModelTopicClass(final Element element) {
		final String model = getModelString(element);
		final String topic = getTopicString(element);
		final String clazz = getClassString(element);
		
		if (model!=null && topic!=null && clazz!=null) {
			return getModelString(element) + "." +
					getTopicString(element) + "." +
					getClassString(element);
		} else {
			return null;
		}
		
	}
	
	public static String getModelString(final Element element) {
		if (element.getContainer(Model.class) != null) {
			return element.getContainer(Model.class).getName();
		} else {
			return null;
		}
	}

	public static String getTopicString(final Element element) {
		if (element.getContainer(Topic.class) != null) {
			return element.getContainer(Topic.class).getName();
		} else {
			return null;
		}
	}
	
	public static String getClassString(final Element element) {
		if (element.getContainer(AbstractClassDef.class) != null) {
			return element.getContainer(AbstractClassDef.class).getName();
		} else {
			return null;
		}
	}
}
