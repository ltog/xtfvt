package ch.htwchur.xtfvt.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Utility methods
 * 
 * @author Martin Studer
 */
public class IomXtfUtilityHelper {
	private static <T> List<T> castList(Class<? extends T> clazz,
			Collection<?> c) {
		List<T> r = new ArrayList<T>(c.size());
		for (Object o : c)
			r.add(clazz.cast(o));
		return r;
	}

	/**
	 * Safe cast of a collection to a ArrayList<String>. This method prevents
	 * unchecked conversion warnings. Example: <code>
	 * ArrayList<String> l = new ArrayList();							// with warning
	 * ArrayList<String> l = collection2ArrayList(new ArrayList());		// without warning
	 * </code>
	 * 
	 * @see http
	 *      ://stackoverflow.com/questions/367626/how-do-i-fix-the-expression
	 *      -of-type-list-needs-unchecked-conversion
	 * 
	 * @param c
	 *            the Collection
	 * @return the ArrayList
	 */
	public static ArrayList<String> collection2ArrayList(Collection<?> c) {
		return (ArrayList<String>) castList(String.class, c);
	}
}
