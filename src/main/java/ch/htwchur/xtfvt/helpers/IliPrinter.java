package ch.htwchur.xtfvt.helpers;

import java.lang.reflect.Field;
import java.util.Iterator;
import ch.interlis.ili2c.metamodel.*;

/**
 * Class used for understanding/debugging the reading of ili files. The class is
 * not necessary for the functioning of xtfvt and may be deleted.
 * 
 * @author ltog
 *
 */
public class IliPrinter {

	public IliPrinter(TransferDescription transferDescription) {
		printIterate(transferDescription, 0);
	}
	
	private static void printIterate(Element element, int level) {
		// print element info
		indent(level);
		System.out.print("getName()=" + element.getName() +
				" ; getClass().getName()=" + element.getClass().getName() + " ;"
				+ " toString()=" + element.toString()
				+ " getContainer()=" + element.getContainer() 
				+ " element.getScopedName()=" + element.getScopedName(null));
		for (Field field : element.getClass().getFields()) {
			try {
				element.getClass().getField(field.getName());
				System.out.print(" field.getName()=" + field.getName());
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (element.getName() != null && element.getName().contains("Point2d")) {
			System.out.println();
			
			Iterator<?> it = ((Container<?>)element.getContainer()).iterator();
			while(it.hasNext()) {
				Element elt = (Element) it.next();
				if (elt instanceof NumericType) {
					System.out.println("asdfasdf = " + ((NumericType) elt).getMaximum());
				}
				
				if (elt instanceof AttributeDef)
				{
					System.out.println("Found instanceof AttributeDef");
				}
				else if (elt instanceof RoleDef)
				{
					System.out.println("Found instanceof RoleDef");
				}
				else if (elt instanceof Function)
				{
					System.out.println("Found instanceof Function");
				}
				else if (elt instanceof Parameter)
				{
					System.out.println("Found instanceof Parameter");
				}
				else if (elt instanceof Domain)
				{
					System.out.println("Found instanceof Domain");
					System.out.println("toString() = " + ((Domain) elt).toString());
					
					if (((Domain)elt).getType() instanceof NumericType) {
						System.out.println("max = " + ((NumericType)((Domain)elt).getType()).getMaximum());
					}
					if (((Domain)elt).getType() instanceof NumericalType) {
						System.out.println("is instanceof NumericalType");
					}
					if (((Domain)elt).getType() instanceof BaseType) {
						System.out.println("is instanceof BaseType");
					}
					if (((Domain)elt).getType() instanceof CoordType) {
						System.out.println("is instanceof CoordType");
						NumericalType[] nlt = (((CoordType)((Domain)elt).getType()).getDimensions());
						if (nlt[0] instanceof NumericType) {
							System.out.println("is NumericType");
						}
						if (nlt[0] instanceof NumericalType) {
							System.out.println("is NumericalType");
						}
						System.out.println(((NumericType)(nlt[0])).getMaximum());
					}
					System.out.println("mandatory = " + ((Domain)elt).getType().isMandatory());
				}
				else if (elt instanceof LineForm)
				{
					System.out.println("Found instanceof LineForm");
				}
				else if (elt instanceof Unit)
				{
					System.out.println("Found instanceof Unit");
				}
				else if (elt instanceof Model)
				{
					System.out.println("\n-----------------\nFound instanceof Model");
				}
				else if (elt instanceof Topic)
				{
					System.out.println("Found instanceof Topic");
				}
				else if (elt instanceof MetaDataUseDef)
				{
					System.out.println("Found instanceof MetaDataUseDef");
				}
				else if (elt instanceof Table)
				{
					System.out.println("Found instanceof Table");
				}
				else if (elt instanceof AssociationDef)
				{
					System.out.println("Found instanceof AssociationDef");
				}
				else if (elt instanceof View)
				{
					System.out.println("Found instanceof View");
				}
				else if (elt instanceof Graphic)
				{
					System.out.println("Found instanceof Graphic");
				}
				else if (elt instanceof Constraint)
				{
					System.out.println("Found instanceof Constraint");
				}
				else if (elt instanceof ExpressionSelection)
				{
					System.out.println("Found instanceof ExpressSelection");
				}
				else if (elt instanceof SignAttribute)
				{
					System.out.println("Found instanceof SignAttribute");
				}
			}
		}
		System.out.println("");
	
		if (element instanceof Container<?>) { // element is a composite
			Iterator<?> it = ((Container<?>)element).iterator();
			while (it.hasNext()) {
				Object obj = it.next();
				printIterate((Element)obj, level+1); // do recursion
			}
		} else { // element is not a composite
			indent(level);
			System.out.println("Element is not instanceof Container<?>");
		}
		
	}
	
	private static void indent(int level){
		for (int i=0; i<level; i++) {
			System.out.print("    ");
		}
	}
}
