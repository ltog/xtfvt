package ch.htwchur.xtfvt.helpers;

import java.util.ArrayList;
import java.util.List;

import ch.htwchur.xtfvt.datastructures.IomObjectWithInfo;
import ch.interlis.iom.IomObject;
import ch.interlis.iom_j.xtf.XtfReader;
import ch.interlis.iox.EndBasketEvent;
import ch.interlis.iox.EndTransferEvent;
import ch.interlis.iox.IoxEvent;
import ch.interlis.iox.IoxException;
import ch.interlis.iox.ObjectEvent;
import ch.interlis.iox.StartBasketEvent;
import ch.interlis.iox.StartTransferEvent;
import static ch.htwchur.xtfvt.helpers.IomBrowser.*;

/**
 * Class used for understanding/debugging the reading of xtf files. The class is
 * not necessary for the functioning of xtfvt and may be deleted.
 * 
 * @author ltog
 *
 */
public class XtfPrinter {

	public XtfPrinter(String path) {
		startAnalyzingObject(path);
		testGetRecursivelyObjectsWithTag(path);
	}
	
	private static void testGetRecursivelyObjectsWithTag(String path) {
		IoxEvent ioxEvent;
		List<IomObjectWithInfo> list = new ArrayList<IomObjectWithInfo>();
		
		XtfReader xtfReader = null;
		try {
			xtfReader = new XtfReader(new java.io.File(path));
			do {
				ioxEvent = xtfReader.read();
				if (ioxEvent instanceof ObjectEvent) {
					list.addAll(IomBrowser.getRecursivelyObjectsWithInfoHavingTag(
							((ObjectEvent)ioxEvent).getIomObject(), "POLYLINE"));
				}
			} while (!(ioxEvent instanceof EndTransferEvent));
		} catch (IoxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (xtfReader != null) {
				try {
					xtfReader.close();
				} catch (IoxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private static void startAnalyzingObject(String path) {
		IoxEvent ioxEvent;
		
		XtfReader xtfReader = null;
		try {
			xtfReader = new XtfReader(new java.io.File(path));
			do {
				ioxEvent = xtfReader.read();

				if (ioxEvent instanceof StartTransferEvent) {

				} else if (ioxEvent instanceof StartBasketEvent){

				} else if (ioxEvent instanceof ObjectEvent) {
					recursivelyAnalyzeObject(((ObjectEvent)ioxEvent).getIomObject(), 0);
				} else if (ioxEvent instanceof EndBasketEvent) {

				} else if (ioxEvent instanceof EndTransferEvent) {

				}
			} while (!(ioxEvent instanceof EndTransferEvent));
		} catch (IoxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (xtfReader != null) {
				try {
					xtfReader.close();
				} catch (IoxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	private static void recursivelyAnalyzeObject(final IomObject iomObject, final int level) {
		indent(level);
		System.out.println("===== BEGIN OBJECT ============= level = " + level);
		
		indent(level);
		System.out.println("Object tag (type of object) = " + iomObject.getobjecttag());
		
		indent(level);
		System.out.println("OID (identity of object) = " + iomObject.getobjectoid());
		
		indent(level);
		System.out.println("Number of attributes = " + iomObject.getattrcount());
		
		indent(level);
		System.out.println("Attributes are:");
		
		//indent(level);
		//System.out.println("Line number = " + iomObject.getobjectline()); // always zero
		
		System.out.println("");
		
		for (int i=0; i<getNumberOfAttributes(iomObject); i++) { // iterate over attributes(=properties)
			indent(level);
			String attributeName = iomObject.getattrname(i);
			System.out.println("- getAttrName(" + i + ") = " + attributeName);
			
			// attributeValue is only defined for primitive attributes
			String attributeValue = iomObject.getattrvalue(attributeName);
			indent(level);
			System.out.println("  getAttrValue(" + attributeName + ") = " + attributeValue);
			
			if (isPrimitiveAttribute(iomObject, attributeName)) {
				indent(level);
				
			} else { // structured attribute
				indent(level);
				System.out.println("  Structured attribute with " + 
						getNumberOfInnerObjects(iomObject, i) + " inner objects");
				// iterate over values (objects) of this attribute
				for (int j=0; j<getNumberOfInnerObjects(iomObject, i); j++) {
					indent(level);
					System.out.println("  Value " + j + ": ");
					IomObject innerIomObject = getInnerObject(iomObject, i, j);
					if (innerIomObject != null) {
						recursivelyAnalyzeObject(innerIomObject, level+1);
					} else {
						indent(level);
						System.out.println("  innerIomObject is null");
					}
				}
			}
			
			System.out.println("");
		}
		
	}

	private static void indent(int level){
		for (int i=0; i<level; i++) {
			System.out.print("    ");
		}
	}

}
