#!/bin/bash

IFS_OLD=$IFS
IFS=","
ORIGINAL_DIR=$(pwd)

echo 
echo -en "DISCLAIMER: You are going to download external files that are not part of xtfvt and are NOT covered by its licence. The files are copyrighted by their respective owners. By continuing you agree that you are legally entitled to download these files and work with them. If you are unsure, do NOT continue. Type 'yes' to acknowledge or anything else to abort.\n\nDo you want to continue? "

read -e ack

if [[ "$ack" == "yes" ]]
then
	cd src/test/resources/ch/htwchur/xtfvt/checks/external

	while read url outname comment
	do
		echo "Downloading $url to $outname"
		wget -O $outname $url

	done < <(egrep '[^,]+,[^,]+' ./files_to_download.csv | tail -n +2) # search for a comma with stuff left and right; skip first line
	#    <   redirect
	#      < process substitution

	cd av_replica && unzip -o test23_20080410.zip ; cd $ORIGINAL_DIR/src/test/resources/ch/htwchur/xtfvt/checks/external

	# Make files processable by ili2c
	patch roads/RoadsExdm2ien-20050616.ili < roads/RoadsExdm2ien-20050616.ili.patch
	patch av_replica/Test23_erweitert.ili < av_replica/Test23_erweitert.ili.patch
	patch av_replica/Test23.ili < av_replica/Test23.ili.patch

fi

cd $ORIGINAL_DIR
IFS=$IFS_OLD
