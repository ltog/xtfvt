# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# 2014-10-23: THIS SCRIPT IS OUT OF DATE; REFER TO
# download-external-xtfs-for-linux.sh TO SEE WHAT IT IS
# SUPPOSED TO DO.
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# To use this script you need to type
#     Set-ExecutionPolicy RemoteSigned
# in a Command Prompt running with Adminstrator privileges once
# to enable the execution of unsigned scripts.
#
# You can undo this change by executing
#     Set-ExecutionPolicy Restricted
#
# To run this file right-click it and choose 'Run with PowerShell'

Write-Host ""
Write-Host "DISCLAIMER: You are going to download external files that are not part of xtfvt and are NOT covered by its licence. The files are copyrighted by their respective owners. By continuing you agree that you are legally entitled to download these files and work with them. If you are unsure, do NOT continue. Type 'yes' to acknowledge or anything else to abort."
Write-Host ""
$ack = Read-Host "Do you want to continue? "

if ($ack -like "yes") {
	$files_to_download = Import-CSV .\files_to_download.csv
	ForEach ( $file_to_download in $files_to_download ) {
		if (!(([string]::IsNullOrEmpty($file_to_download.Url)) -or ([string]::IsNullOrEmpty($file_to_download.Outname)))){ # allow empty lines in csv file
			Write-Host Downloading $file_to_download.Url to $file_to_download.Outname
			(New-Object System.Net.WebClient).DownloadFile($file_to_download.Url,$file_to_download.Outname)
		}
	}
}
